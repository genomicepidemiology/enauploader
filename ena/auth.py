class Auth():
    """ Simple object to load and store account credentials.
    """

    def __init__(self, credentials=None):
        if credentials:
            with open(credentials, "r") as fh:
                self.user = fh.readline().rstrip()
                self.password = fh.readline().rstrip()
        else:
            self.user = None
            self.password = None

    def get_rest_auth(self):
        return "auth=ENA%20" + self.user + "%20" + self.password
