import os.path
import requests
from .exml import ENAXML
from xml.etree import ElementTree as ET


class ENAExperimentXML(ENAXML):

    def __init__(self, project):
        ENAXML.__init__(self)

        self.create_root("EXPERIMENT_SET")
        self.root.set("xsi:noNamespaceSchemaLocation", ("ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/" "SRA.experiment.xsd"))

        for exp in project.experiments:

            # Experiment header
            xml_exp = ET.SubElement(self.root, "EXPERIMENT")
            xml_exp.set("alias", exp.alias)
            xml_exp.set("center_name", project.center)
            prj_ref = ET.SubElement(xml_exp, "STUDY_REF")
            prj_ref.set("refname", project.alias)

            # DESIGN block
            design = ET.SubElement(xml_exp, "DESIGN")

            design_desc = ET.SubElement(design, "DESIGN_DESCRIPTION")
            if exp.description:
                design_desc.text = exp.description

            for sample in exp.samples.keys():
                sample_desc = ET.SubElement(design, "SAMPLE_DESCRIPTOR")
                sample_desc.set("refname", exp.samples[sample].alias)

            lib_desc = ET.SubElement(design, "LIBRARY_DESCRIPTOR")
            lib_name = ET.SubElement(lib_desc, "LIBRARY_NAME")
            lib_name.text = "unspecified"
            lib_strat = ET.SubElement(lib_desc, "LIBRARY_STRATEGY")
            lib_strat.text = exp.lib_strat
            lib_src = ET.SubElement(lib_desc, "LIBRARY_SOURCE")
            lib_src.text = exp.lib_source
            lib_sel = ET.SubElement(lib_desc, "LIBRARY_SELECTION")
            lib_sel.text = exp.lib_sel
            lib_lay = ET.SubElement(lib_desc, "LIBRARY_LAYOUT")
            if exp.lib_layout == "PAIRED":
                lib_pair = ET.SubElement(lib_lay, "PAIRED")
                if exp.insert_size:
                    lib_pair.set("NOMINAL_LENGTH", str(exp.insert_size))
            else:
                lib_pair = ET.SubElement(lib_lay, "SINGLE")

            # PLATFORM

            plat = ET.SubElement(xml_exp, "PLATFORM")
            spec_plat = ET.SubElement(plat, exp.platform.upper())
            model = ET.SubElement(spec_plat, "INSTRUMENT_MODEL")
            model.text = "unspecified"
            if exp.instrument_model:
                model.text = exp.instrument_model
