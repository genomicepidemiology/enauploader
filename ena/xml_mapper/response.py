import os.path
import requests
from xml.etree import ElementTree as ET


class ENAResponse():

    def __init__(self, response):
        root = ET.fromstring(response)

        self.response_date = root.attrib["receiptDate"]

        success = root.attrib["success"]
        if success == "true":
            self.success = True
        else:
            self.success = False

        # Check if submission was a validate action
        validate = False

        for type_response in root:
            if type_response.tag == "ACTIONS" and type_response.text == "VALIDATE":
                validate = True
                break

        # Initiate storage variables
        self.validate = validate
        self.messages = []
        self.accessions = {}
        self.accessions["EXPERIMENT"] = {}
        self.accessions["RUN"] = {}
        self.accessions["SAMPLE"] = {}
        self.accessions["PROJECT"] = {}
        self.accessions["SUBMISSION"] = {}

        # Retrieve all accession numbers and messages
        for type_response in root:
            if type_response.tag == "PROJECT" and success and not validate:
                if self.success:
                    self.accessions["PROJECT"] = type_response.attrib["accession"]

            if type_response.tag == "SUBMISSION" and success and not validate:
                if self.success:
                    self.accessions["SUBMISSION"] = type_response.attrib["accession"]

            if type_response.tag == "SAMPLE" and success and not validate:
                if self.success:
                    self.accessions["SAMPLE"][type_response.attrib["alias"]] = (type_response.attrib["accession"])

            if type_response.tag == "EXPERIMENT" and success and not validate:
                if self.success:
                    self.accessions["EXPERIMENT"][type_response.attrib["alias"]] = (type_response.attrib["accession"])

            if type_response.tag == "RUN" and success and not validate:
                if self.success:
                    self.accessions["RUN"][type_response.attrib["alias"]] = (type_response.attrib["accession"])

            if type_response.tag == "MESSAGES":
                for message in type_response:
                    if message.tag == "ERROR":
                        self.messages.append("enaresponse-error:" + message.text)
                    if message.tag == "INFO":
                        self.messages.append("enaresponse-info:" + message.text)
