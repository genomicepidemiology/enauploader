import os.path
import requests
from xml.etree import ElementTree as ET
from .response import ENAResponse


class ENAXML():
    """ Abstract class. Although not in its exact definition, as it doesn't
        contain any abstract methods.
        All other ENA<xxx>XML classes inherits this class.
        The class provides a method to create the root XML element and a
        static method to submit xml files to ENA.
    """

    def __init__(self):
        self.root = None
        self.root_tree = None

    def create_root(self, root_tag):
        self.root = ET.Element(root_tag)
        self.root.set("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        self.root_tree = ET.ElementTree(self.root)

    @staticmethod
    def submit(auth, sub_xml, prj_xml, sample_xml, exp_xml, run_xml, test_server=True):
        baseurl = "https://www-test.ebi.ac.uk/ena/submit/drop-box/submit/?"
        if not test_server:
            baseurl = "https://www.ebi.ac.uk/ena/submit/drop-box/submit/?"
        print('Uploading to !!!', baseurl)
        auth_url = auth.get_rest_auth()
        url = baseurl + auth_url

        sub_filename = os.path.basename(sub_xml)
        prj_filename = os.path.basename(prj_xml)
        exp_filename = os.path.basename(exp_xml)
        run_filename = os.path.basename(run_xml)
        sample_filename = os.path.basename(sample_xml)

        sub_files = [
            ('SUBMISSION', (sub_filename, open(sub_xml, 'rb'))),
            ('PROJECT', (prj_filename, open(prj_xml, 'rb'))),
            ('SAMPLE', (sample_filename, open(sample_xml, 'rb'))),
            ('RUN', (run_filename, open(run_xml, 'rb'))),
            ('EXPERIMENT', (exp_filename, open(exp_xml, 'rb')))
        ]

        response = requests.post(url, files=sub_files)

        if(response.status_code == 404):
            raise OSError("HTTP Status 404 - " + baseurl)

        return ENAResponse(response=response.text)
