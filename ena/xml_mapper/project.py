import os.path
import requests
from .exml import ENAXML
from xml.etree import ElementTree as ET


class ENAProjectXML(ENAXML):

    def __init__(self, project):
        ENAXML.__init__(self)

        self.create_root("PROJECT_SET")
        self.root.set("xsi:noNamespaceSchemaLocation", ("ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/" "SRA.project.xsd"))

        # Project header
        xml_prj = ET.SubElement(self.root, "PROJECT")
        xml_prj.set("alias", project.alias)
        xml_prj.set("center_name", project.center)

        title = ET.SubElement(xml_prj, "TITLE")
        title.text = project.title
        desc = ET.SubElement(xml_prj, "DESCRIPTION")
        desc.text = project.abstract

        sub_prj = ET.SubElement(xml_prj, "SUBMISSION_PROJECT")
        sub_prj.set("material", project.material)
        sub_prj.set("selection", project.selection)
        sub_prj.set("scope", project.scope)
        ET.SubElement(sub_prj, "SEQUENCING_PROJECT")
        obj1 = ET.SubElement(sub_prj, "OBJECTIVE")
        obj1.text = "sequence"
        obj2 = ET.SubElement(sub_prj, "OBJECTIVE")
        obj2.text = "raw sequence"

        if project.umbrella_acc:
            rel_projects = ET.SubElement(xml_prj, "RELATED_PROJECTS")
            rel_prj = ET.SubElement(rel_projects, "RELATED_PROJECT")
            parent_prj = ET.SubElement(rel_prj, "PARENT_PROJECT")
            parent_prj.set("accession", project.umbrella_acc)
