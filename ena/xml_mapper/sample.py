import os.path
import requests
from .exml import ENAXML
from xml.etree import ElementTree as ET


class ENASampleXML(ENAXML):

    def __init__(self, project, ena_checklist_id):
        ENAXML.__init__(self)

        self.create_root("SAMPLE_SET")
        self.root.set("xsi:noNamespaceSchemaLocation", ("ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/" "SRA.sample.xsd"))

        for sample in project.sample_order:

            # SAMPLE header
            xml_sample = ET.SubElement(self.root, "SAMPLE")
            xml_sample.set("alias", sample.alias)
            xml_sample.set("center_name", project.center)

            # SAMPLE NAME
            xml_sample_name = ET.SubElement(xml_sample, "SAMPLE_NAME")
            taxon_id = ET.SubElement(xml_sample_name, "TAXON_ID")
            taxon_id.text = sample.taxon_id
            sci_name = ET.SubElement(xml_sample_name, "SCIENTIFIC_NAME")
            sci_name.text = sample.sci_name

            # SAMPLE ATTRIBUTES
            sample_attr = ET.SubElement(xml_sample, "SAMPLE_ATTRIBUTES")

            # Host attributes
            host_sci_name = self.create_sample_attr("host scientific name", sample.host)
            sample_attr.append(host_sci_name)
            host_status = self.create_sample_attr("host health state", sample.host_status)
            sample_attr.append(host_status)

            if sample.host_taxon_id:
                host_aso = self.create_sample_attr("Is the sequenced pathogen host associated?", "Yes")
                if sample.isolation_source:
                    iso_src = self.create_sample_attr("isolation source host-associated", sample.isolation_source)
                    sample_attr.append(iso_src)
                    non_src = self.create_sample_attr("isolation source non-host-associated")
                    sample_attr.append(non_src)
            else:
                host_aso = self.create_sample_attr(
                    "Is the sequenced pathogen host associated?", "No")
                if sample.isolation_source:
                    iso_src = self.create_sample_attr("isolation source host-associated")
                    sample_attr.append(iso_src)
                    non_src = self.create_sample_attr("isolation source non-host-associated", sample.isolation_source)
                    sample_attr.append(non_src)
            sample_attr.append(host_aso)

            # Source attributes
            if project.prj_type == "isolate":
                env = self.create_sample_attr("environmental_sample", "No")
                sample_attr.append(env)
            elif project.prj_type == "metagenomic":
                env = self.create_sample_attr("environmental_sample", "Yes")
                sample_attr.append(env)

            # Sampling attributes
            if sample.collected_by:
                col_by = self.create_sample_attr("collected_by", sample.collected_by)
                sample_attr.append(col_by)
            if sample.collection_date:
                col_dat = self.create_sample_attr("collection date", sample.collection_date)
                sample_attr.append(col_dat)

            # if sample.geo_loc_name:
            #    loc_name = self.create_sample_attr("geo_loc_name", sample.geo_loc_name)
            #    sample_attr.append(loc_name)

            if sample.country:
                country = self.create_sample_attr("geographic location (country and/or sea)", sample.country)
                sample_attr.append(country)
            if sample.lon and sample.lat:
                lon = self.create_sample_attr("geographic location (longitude)", str(sample.lon), units="DD")
                sample_attr.append(lon)
                lat = self.create_sample_attr("geographic location (latitude)", str(sample.lat), units="DD")
                sample_attr.append(lat)

            # Identification
            if sample.isolate:
                isolate = self.create_sample_attr("isolate", sample.isolate)
                sample_attr.append(isolate)
            if sample.strain:
                strain = self.create_sample_attr("strain", sample.strain)
                sample_attr.append(strain)

            # Value is empty if no serovar has been given.
            serovar = self.create_sample_attr("serovar", sample.serovar)
            sample_attr.append(serovar)

            if ena_checklist_id:
                check = self.create_sample_attr("ENA-CHECKLIST", ena_checklist_id)
                sample_attr.append(check)

    @staticmethod
    def create_sample_attr(tag, value=None, units=None):
        s_attr = ET.Element("SAMPLE_ATTRIBUTE")
        tag_s_attr = ET.SubElement(s_attr, "TAG")
        tag_s_attr.text = tag
        val_s_attr = ET.SubElement(s_attr, "VALUE")
        if value:
            val_s_attr.text = value
        if units:
            uni_s_attr = ET.SubElement(s_attr, "UNITS")
            uni_s_attr.text = units

        return s_attr
