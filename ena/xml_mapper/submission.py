import os.path
import requests
from .exml import ENAXML
from xml.etree import ElementTree as ET


class ENASubmissionXML(ENAXML):

    def __init__(self, project, prj_xml, sample_xml, exp_xml, run_xml):
        ENAXML.__init__(self)

        prj_xml = os.path.basename(prj_xml)
        sample_xml = os.path.basename(sample_xml)
        exp_xml = os.path.basename(exp_xml)
        run_xml = os.path.basename(run_xml)

        self.create_root("SUBMISSION_SET")
        self.root.set("xsi:noNamespaceSchemaLocation", ("ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/" "SRA.submission.xsd"))

        # Submission header
        xml_sub = ET.SubElement(self.root, "SUBMISSION")
        xml_sub.set("alias", project.alias)
        xml_sub.set("center_name", project.center)

        # ACTIONS block

        actions = ET.SubElement(xml_sub, "ACTIONS")
        action = "ADD"
        if project.validate:
            action = "VALIDATE"

        act_prj = self.create_action_attr(action=action, filename=prj_xml, schema="project")
        act_sample = self.create_action_attr(action=action, filename=sample_xml, schema="sample")
        act_exp = self.create_action_attr(action=action, filename=exp_xml, schema="experiment")
        act_run = self.create_action_attr(action=action, filename=run_xml, schema="run")

        actions.append(act_prj)
        actions.append(act_sample)
        actions.append(act_exp)
        actions.append(act_run)

        if project.release_date == "release":
            act_rel = self.create_action_attr(action="RELEASE")
            actions.append(act_rel)
        else:
            act = ET.SubElement(actions, "ACTION")
            act_hold = ET.SubElement(act, "HOLD")
            act_hold.set("HoldUntilDate", project.release_date)

    @staticmethod
    def create_action_attr(action, filename=None, schema=None):
        xml_action = ET.Element("ACTION")
        if action == "ADD":
            xml_add = ET.SubElement(xml_action, "ADD")
            xml_add.set("source", filename)
            xml_add.set("schema", schema)
        elif action == "VALIDATE":
            xml_validate = ET.SubElement(xml_action, "VALIDATE")
            xml_validate.set("source", filename)
            xml_validate.set("schema", schema)
        elif action == "RELEASE":
            ET.SubElement(xml_action, "RELEASE")
        elif action == "HOLD":
            ET.SubElement(xml_action, "HOLD")

        return xml_action
