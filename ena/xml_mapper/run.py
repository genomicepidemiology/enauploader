import os.path
import requests
from xml.etree import ElementTree as ET
from .exml import ENAXML


class ENARunXML(ENAXML):

    def __init__(self, project):
        ENAXML.__init__(self)

        self.create_root("RUN_SET")
        self.root.set("xsi:noNamespaceSchemaLocation", ("ftp://ftp.sra.ebi.ac.uk/meta/xsd/sra_1_5/" "SRA.run.xsd"))

        for sample in project.sample_order:
            for run in sample.run:
                # RUN header
                xml_run = ET.SubElement(self.root, "RUN")
                xml_run.set("alias", run.alias)
                xml_run.set("center_name", project.center)

                # Experiment reference
                exp_ref = ET.SubElement(xml_run, "EXPERIMENT_REF")
                exp_ref.set("refname", run.experiment.alias)

                # Sequence data information
                data = ET.SubElement(xml_run, "DATA_BLOCK")
                files_data = ET.SubElement(data, "FILES")
                file_files_data1 = ET.SubElement(files_data, "FILE")
                filename1 = os.path.basename(run.file1)
                file_files_data1.set("filename", project.upload_dir + "/" + filename1)
                file_files_data1.set("filetype", run.filetype)
                file_files_data1.set("checksum_method", "MD5")
                file_files_data1.set("checksum", run.file1_md5)
                if run.file2:
                    file_files_data1 = ET.SubElement(files_data, "FILE")
                    filename2 = os.path.basename(run.file2)
                    file_files_data1.set("filename", project.upload_dir + "/" + filename2)
                    file_files_data1.set("filetype", run.filetype)
                    file_files_data1.set("checksum_method", "MD5")
                    file_files_data1.set("checksum", run.file2_md5)
