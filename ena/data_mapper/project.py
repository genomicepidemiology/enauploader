import argparse
import sys
import time
import urllib.parse
import urllib
import requests
import random
import hashlib
import os.path
from ..checklist.min_check import ENAMinCheck
from ..checklist.gmin_check import GMIMinCheck


class Project():
    ensembl_server = "http://rest.ensembl.org/taxonomy/id/"
    openstreetmap_server = "http://nominatim.openstreetmap.org/search?"

    def __init__(self, center, title, abstract, material, selection, prj_type,
                 release_date, broker=None, umbrella_acc=None, validate=True,
                 md5sums=None, ena_upload_dir=None):

        self.samples = {}
        self.experiments = []
        self.sample_order = []
        self.time = time.time()
        self.alias = ("Project_" + format(self.time, '.6f'))
        self.center = center
        self.title = title
        self.abstract = abstract
        self.material = material
        self.selection = selection
        self.prj_type = prj_type
        self.transl_sci_name = {}

        if prj_type == "isolate":
            self.scope = "single isolate"
        elif prj_type == "metagenomic":
            self.scope = "community"
        else:
            self.scope = "other"

        self.release_date = release_date
        self.broker = broker
        self.umbrella_acc = umbrella_acc
        self.validate = validate

        # Destination folder on the ENA server
        if(ena_upload_dir is None):
            self.upload_dir = self.alias
        else:
            self.upload_dir = ena_upload_dir

        if(md5sums):
            self.md5sums = {}
            with open(md5sums, "r") as fh:
                for line in fh:
                    line = line.strip()
                    if(not line):
                        continue
                    # print("DEBUG line 59, line: " + line)
                    md5, file = line.split("  ")
                    filename = os.path.basename(file)
                    self.md5sums[filename] = md5
        else:
            self.md5sums = None

    def add_sample(self, sample, experiment, run):
        self.samples[sample.alias] = sample
        self.sample_order.append(sample)

        # Check if similar experiment object already exists
        create_new_experiment = True
        for exp in self.experiments:
            if exp == experiment:
                experiment = exp
                create_new_experiment = False
                break

        experiment.samples[sample.alias] = sample
        experiment.runs.append(run)
        sample.experiment.append(experiment)
        sample.run.append(run)
        run.experiment = experiment

        if create_new_experiment:
            self.experiments.append(experiment)

        # Create automatic title and abstract. Only possible if project
        # contains a single sample.
        if(self.title is None or self.abstract is None):
            sample_aliases = self.samples.keys()

            if(len(sample_aliases) > 1):
                err_msg = ("Either Title or Abstract is empty. Both must "
                           "contain text.\nNOTE: An automatic title and/or "
                           "abstract can be created, but it requires a project"
                           " to only hold a single sample.")
                raise(MissingDataError(err_msg))

            # Automatic title for Single isolates.
            if(self.title is None and self.scope == "single isolate"):
                strain_name = sample.strain
                if(strain_name is None):
                    strain_name = "No strain name provided"

                self.title = ("Automatic upload of a(n) " + sample.sci_name
                              + " isolate, strain: " + strain_name + ".")
            # Automatic title for Metagenomes.
            elif(self.title is None and self.scope == "community"):
                self.title = ("Automatic upload of a(n) "
                              + sample.isolation_source
                              + " metagenome sample (" + self.material + ").")

            # Automatic abstract for Single isolates and Metagenomes.
            if(self.abstract is None):
                collected_by = sample.collected_by
                if(collected_by is None):
                    collected_by = "not provided"

                self.abstract = ("This isolate was automatically uploaded to "
                                 "the European Nucleotide Archive through the "
                                 "COMPARE platform "
                                 "(https://compare.cbs.dtu.dk). The isolate "
                                 "was collected by: " + collected_by + ".")

    def check_data(self, checklist):
        """ Will test the loaded project data against the given checklist. The
            given checklist should be the name of one of the checklists
            implemented in the module enachecklists.
        """
        if checklist == "minimum":
            report = ENAMinCheck(project=self)
        elif checklist == "gmi":
            report = GMIMinCheck(project=self)

        return report


class MissingDataError(Exception):
    """ Raise when encountering None objects or empty strings where
        data was expected
    """
    def __init__(self, message, *args):
        self.message = message
        # allow users initialize misc. arguments as any other builtin Error
        super(MissingDataError, self).__init__(message, *args)
