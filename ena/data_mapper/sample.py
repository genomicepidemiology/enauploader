import argparse
import sys
import time
import urllib.parse
import urllib
import requests
import random
import hashlib
import time

from .project import Project
from ..checklist.min_check import ENAMinCheck
from ..checklist.gmin_check import GMIMinCheck


class Sample():

    def __init__(self, project, name, organism, uuid=None, lat=None, lon=None,
                 geo_loc_name=None, country=None, collection_date=None,
                 host=None, host_status=None, collected_by=None,
                 isolation_source=None, isolate=None, strain=None,
                 serovar=None):
        self.experiment = []
        self.run = []

        name = urllib.parse.quote(name)
        if uuid:
            self.alias = uuid
        else:
            self.alias = ("Sample_" + format(time.time(), '.6f') + "_" + name)

        self.project = project
        self.name = name

        # ENSEMBL server is queried with the organism and host name
        # provided, to get taxon id and the scientific name.
        # A dict is kept of the query, so identical queries are not needed.
        if(organism in project.transl_sci_name):
            self.sci_name, self.taxon_id = project.transl_sci_name[organism]
        else:
            self.sci_name, self.taxon_id = self.get_sciname_and_id(organism)
            project.transl_sci_name[organism] = (self.sci_name, self.taxon_id)

        if host:
            if(host in project.transl_sci_name):
                self.host, self.host_taxon_id = project.transl_sci_name[host]
            else:
                self.host, self.host_taxon_id = self.get_sciname_and_id(host)
                project.transl_sci_name[host] = (self.host, self.host_taxon_id)
            self.host_status = host_status
        else:
            self.host = "not applicable"
            self.host_taxon_id = None
            self.host_status = "not applicable"

        if country:
            # Converts ISO 3-letter country code to country names
            self.country = Sample.get_country_from_iso(country)
            # If country wasnt a code self.country is None, and is set to
            # country
            if(self.country is None):
                self.country = country

        if geo_loc_name:
            geo_loc_list = geo_loc_name.split(":")
            country_name = Sample.get_country_from_iso(geo_loc_list[0])
            if(country_name is not None):
                geo_loc_list[0] = country_name
                geo_loc_name = ":".join(geo_loc_list)
            if not country:
                self.country = geo_loc_list[0]

        self.lat = lat
        self.lon = lon

        # Retrieves coordinates of the region or country given if no
        # coordinates have been provided.
        if (not lat or not lon) and geo_loc_name:
            # Country is assumed if no colons are used.
            # Colons are used to seperate regions as written in the MIxS
            # specification. Ex.: "Denmark:Copenhagen"
            query_type = "country="
            if len(geo_loc_name.split(":")) > 1:
                query_type = "q="
                openstreetmap_query = geo_loc_name.replace(":", ",")
            else:
                openstreetmap_query = geo_loc_name

            openstreetmap_query = urllib.parse.quote(openstreetmap_query)
            openstreetmap_query = (project.openstreetmap_server + query_type + openstreetmap_query + "&format=json")

            r = requests.get(openstreetmap_query, headers={"Content-Type": "application/json"})

            if r.ok:
                openstreetmap_response = r.json()

                # Only use coordinates if a single result is returned
                if len(openstreetmap_response) == 1:
                    self.lat = openstreetmap_response[0]["lat"]
                    self.lon = openstreetmap_response[0]["lon"]

        self.geo_loc_name = geo_loc_name
        self.collection_date = collection_date
        self.collected_by = collected_by
        self.isolation_source = isolation_source
        self.isolate = isolate
        self.strain = strain
        self.serovar = serovar

    @staticmethod
    def get_sciname_and_id(query):
        """ Looks for query using ensembl REST API and returns
            scientific name and taxon id (tuple).
            Query can be the common or scientific name for an organism.
        """
        ensembl_query = Project.ensembl_server + query + "?content-type=application/json"

        try:
            r = requests.get(ensembl_query, headers={"Content-Type": "application/json"})
            # Make sure less than 15 requests per second (ENSEMBL rule).
            time.sleep(1)
        except Exception as e:
            print("Error while interacting with ENA API")
            raise Exception("Error while interacting with ENA API")
            sys.exit()

        if not r.ok:
            print("Possible source of error: Missing or misspelling of organism name. You may need to input the scientific name of the organism or host.")
            print("Query was: " + query)
            print("r url: " + str(r.url))
            print("r text: " + str(r.text))
            raise Exception("Possible source of error: Missing or misspelling of organism name. You may need to input the scientific name of the organism or host.")
            sys.exit()

        ensembl_response = r.json()

        return (ensembl_response["scientific_name"], ensembl_response["id"])

    @staticmethod
    def get_country_from_iso(iso):
        """
        """
        iso_transl = {
            "ABW": "Aruba",
            "AFG": "Afghanistan",
            "AGO": "Angola",
            "AIA": "Anguilla",
            "ALA": "Aaland Islands",
            "ALB": "Albania",
            "AND": "Andorra",
            "ARE": "United Arab Emirates",
            "ARG": "Argentina",
            "ARM": "Armenia",
            "ASM": "American Samoa",
            "ATA": "Antarctica",
            "ATF": "French SouthernTerritories",
            "ATG": "Antigua and Barbuda",
            "AUS": "Australia",
            "AUT": "Austria",
            "AZE": "Azerbaijan",
            "BDI": "Burundi",
            "BEL": "Belgium",
            "BEN": "Benin",
            "BES": "Bonaire, Sint Eustatius and Saba",
            "BFA": "Burkina Faso",
            "BGD": "Bangladesh",
            "BGR": "Bulgaria",
            "BHR": "Bahrain",
            "BHS": "Bahamas",
            "BIH": "Bosnia and Herzegovina",
            "BLM": "Saint Barthelemy",
            "BLR": "Belarus",
            "BLZ": "Belize",
            "BMU": "Bermuda",
            "BOL": "Bolivia, Plurinational State of",
            "BRA": "Brazil",
            "BRB": "Barbados",
            "BRN": "Brunei Darussalam",
            "BTN": "Bhutan",
            "BVT": "Bouvet Island",
            "BWA": "Botswana",
            "CAF": "Central African Republic",
            "CAN": "Canada",
            "CCK": "Cocos (Keeling) Islands",
            "CHE": "Switzerland",
            "CHL": "Chile",
            "CHN": "China",
            "CIV": "Cote d'Ivoire",
            "CMR": "Cameroon",
            "COD": "Congo, the Democratic Republic of the",
            "COG": "Congo",
            "COK": "Cook Islands",
            "COL": "Colombia",
            "COM": "Comoros",
            "CPV": "Cabo Verde",
            "CRI": "Costa Rica",
            "CUB": "Cuba",
            "CUW": "Curacao",
            "CXR": "Christmas Island",
            "CYM": "Cayman Islands",
            "CYP": "Cyprus",
            "CZE": "Czech Republic",
            "DEU": "Germany",
            "DJI": "Djibouti",
            "DMA": "Dominica",
            "DNK": "Denmark",
            "DOM": "Dominican Republic",
            "DZA": "Algeria",
            "ECU": "Ecuador",
            "EGY": "Egypt",
            "ERI": "Eritrea",
            "ESH": "Western Sahara",
            "ESP": "Spain",
            "EST": "Estonia",
            "ETH": "Ethiopia",
            "FIN": "Finland",
            "FJI": "Fiji",
            "FLK": "Falkland Islands (Malvinas)",
            "FRA": "France",
            "FRO": "Faroe Islands",
            "FSM": "Micronesia, Federated States of",
            "GAB": "Gabon",
            "GBR": "United Kingdom",
            "GEO": "Georgia",
            "GGY": "Guernsey",
            "GHA": "Ghana",
            "GIB": "Gibraltar",
            "GIN": "Guinea",
            "GLP": "Guadeloupe",
            "GMB": "Gambia",
            "GNB": "Guinea-Bissau",
            "GNQ": "Equatorial Guinea",
            "GRC": "Greece",
            "GRD": "Grenada",
            "GRL": "Greenland",
            "GTM": "Guatemala",
            "GUF": "French Guiana",
            "GUM": "Guam",
            "GUY": "Guyana",
            "HKG": "Hong Kong",
            "HMD": "Heard Island and McDonald Islands",
            "HND": "Honduras",
            "HRV": "Croatia",
            "HTI": "Haiti",
            "HUN": "Hungary",
            "IDN": "Indonesia",
            "IMN": "Isle of Man",
            "IND": "India",
            "IOT": "British IndianOcean Territory",
            "IRL": "Ireland",
            "IRN": "Iran, Islamic Republic of",
            "IRQ": "Iraq",
            "ISL": "Iceland",
            "ISR": "Israel",
            "ITA": "Italy",
            "JAM": "Jamaica",
            "JEY": "Jersey",
            "JOR": "Jordan",
            "JPN": "Japan",
            "KAZ": "Kazakhstan",
            "KEN": "Kenya",
            "KGZ": "Kyrgyzstan",
            "KHM": "Cambodia",
            "KIR": "Kiribati",
            "KNA": "Saint Kitts and Nevis",
            "KOR": "Korea, Republic of",
            "KWT": "Kuwait",
            "LAO": "Lao People's Democratic Republic",
            "LBN": "Lebanon",
            "LBR": "Liberia",
            "LBY": "Libya",
            "LCA": "Saint Lucia",
            "LIE": "Liechtenstein",
            "LKA": "Sri Lanka",
            "LSO": "Lesotho",
            "LTU": "Lithuania",
            "LUX": "Luxembourg",
            "LVA": "Latvia",
            "MAC": "Macao",
            "MAF": "Saint Martin (French part)",
            "MAR": "Morocco",
            "MCO": "Monaco",
            "MDA": "Moldova, Republic of",
            "MDG": "Madagascar",
            "MDV": "Maldives",
            "MEX": "Mexico",
            "MHL": "Marshall Islands",
            "MKD": "Macedonia, the former Yugoslav Republic of",
            "MLI": "Mali",
            "MLT": "Malta",
            "MMR": "Myanmar",
            "MNE": "Montenegro",
            "MNG": "Mongolia",
            "MNP": "Northern Mariana Islands",
            "MOZ": "Mozambique",
            "MRT": "Mauritania",
            "MSR": "Montserrat",
            "MTQ": "Martinique",
            "MUS": "Mauritius",
            "MWI": "Malawi",
            "MYS": "Malaysia",
            "MYT": "Mayotte",
            "NAM": "Namibia",
            "NCL": "New Caledonia",
            "NER": "Niger",
            "NFK": "Norfolk Island",
            "NGA": "Nigeria",
            "NIC": "Nicaragua",
            "NIU": "Niue",
            "NLD": "Netherlands",
            "NOR": "Norway",
            "NPL": "Nepal",
            "NRU": "Nauru",
            "NZL": "New Zealand",
            "OMN": "Oman",
            "PAK": "Pakistan",
            "PAN": "Panama",
            "PCN": "Pitcairn",
            "PER": "Peru",
            "PHL": "Philippines",
            "PLW": "Palau",
            "PNG": "Papua New Guinea",
            "POL": "Poland",
            "PRI": "Puerto Rico",
            "PRK": "Korea, Democratic People's Republic of",
            "PRT": "Portugal",
            "PRY": "Paraguay",
            "PSE": "Palestine, State of",
            "PYF": "French Polynesia",
            "QAT": "Qatar",
            "REU": "Reunion",
            "ROU": "Romania",
            "RUS": "Russian Federation",
            "RWA": "Rwanda",
            "SAU": "Saudi Arabia",
            "SDN": "Sudan",
            "SEN": "Senegal",
            "SGP": "Singapore",
            "SGS": "South Georgia and the South Sandwich Islands",
            "SHN": "Saint Helena, Ascension and Tristan da Cunha",
            "SJM": "Svalbard and Jan Mayen",
            "SLB": "Solomon Islands",
            "SLE": "Sierra Leone",
            "SLV": "El Salvador",
            "SMR": "San Marino",
            "SOM": "Somalia",
            "SPM": "Saint Pierre and Miquelon",
            "SRB": "Serbia",
            "SSD": "South Sudan",
            "STP": "Sao Tome and Principe",
            "SUR": "Suriname",
            "SVK": "Slovakia",
            "SVN": "Slovenia",
            "SWE": "Sweden",
            "SWZ": "Swaziland",
            "SXM": "Sint Maarten (Dutch part)",
            "SYC": "Seychelles",
            "SYR": "Syrian Arab Republic",
            "TCA": "Turks and Caicos Islands",
            "TCD": "Chad",
            "TGO": "Togo",
            "THA": "Thailand",
            "TJK": "Tajikistan",
            "TKL": "Tokelau",
            "TKM": "Turkmenistan",
            "TLS": "Timor-Leste",
            "TON": "Tonga",
            "TTO": "Trinidad and Tobago",
            "TUN": "Tunisia",
            "TUR": "Turkey",
            "TUV": "Tuvalu",
            "TWN": "Taiwan, Province of China",
            "TZA": "Tanzania, United Republic of",
            "UGA": "Uganda",
            "UKR": "Ukraine",
            "UMI": "United States Minor Outlying Islands",
            "URY": "Uruguay",
            "USA": "USA",
            "UZB": "Uzbekistan",
            "VAT": "Holy See (Vatican City State)",
            "VCT": "Saint Vincent and the Grenadines",
            "VEN": "Venezuela, Bolivarian Republic of",
            "VGB": "Virgin Islands, British",
            "VIR": "Virgin Islands, U.S.",
            "VNM": "Viet Nam",
            "VUT": "Vanuatu",
            "WLF": "Wallis and Futuna",
            "WSM": "Samoa",
            "YEM": "Yemen",
            "ZAF": "South Africa",
            "ZMB": "Zambia",
            "ZWE": "Zimbabwe"
        }

        return iso_transl.get(iso, None)
