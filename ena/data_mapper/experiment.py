import sys
import urllib
import urllib.parse
import random
import hashlib
import requests
import argparse
from ..checklist.min_check import ENAMinCheck
from ..checklist.gmin_check import GMIMinCheck


class Experiment():

    def __init__(self, exp_id, project, platform, lib_layout,
                 instrument_model=None, insert_size=None, lib_strat="WGS",
                 lib_sel="RANDOM", description=None):
        """ All values are from a controled vocabulary found at ENA:
            https://www.ebi.ac.uk/ena/submit/preparing-xmls#experiment

            Insert size must be provided if lib_layout == "PAIRED".
        """

        self.exp_id = str(exp_id)
        self.samples = {}
        self.runs = []
        self.alias = ("Exp_" + format(project.time, '.6f') + "_" + self.exp_id)
        self.project = project

        if project.prj_type == "isolate":
            self.lib_source = "GENOMIC"
        elif project.prj_type == "metagenomic":
            self.lib_source = "METAGENOMIC"
        else:
            print("ERROR: Did not recognise project type")
            quit(1)

        self.lib_layout = lib_layout.upper()
        if self.lib_layout == "PAIRED":
            if insert_size:
                self.insert_size = insert_size
            else:
                self.insert_size = None

        self.platform = platform
        self.instrument_model = instrument_model
        self.lib_strat = lib_strat
        self.lib_sel = lib_sel
        self.description = description

    def __eq__(self, other):
        if isinstance(other, Experiment):

            if self.project.alias != other.project.alias:
                return False
            if self.exp_id != other.exp_id:
                return False
            if self.lib_layout != other.lib_layout:
                return False
            if self.insert_size != other.insert_size:
                return False
            if self.platform != other.platform:
                return False
            if self.instrument_model != other.instrument_model:
                return False
            if self.lib_strat != other.lib_strat:
                return False
            if self.lib_sel != other.lib_sel:
                return False

            return True

        return NotImplemented

    def __ne__(self, other):
        result = self.__eq__(other)

        if result is NotImplemented:
            return result

        return not result
