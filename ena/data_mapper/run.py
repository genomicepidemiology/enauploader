import sys
import time
import urllib.parse
import urllib
import requests
import random
import hashlib
import argparse
import os.path
from ..checklist.min_check import ENAMinCheck
from ..checklist.gmin_check import GMIMinCheck


class Run():

    run_counter = 0

    def __init__(self, project, file1, file2=None, filetype="fastq"):
        Run.run_counter += 1
        self.experiment = None
        self.file1 = file1
        self.file2 = file2
        self.alias = ("Run_" + format(time.time(), '.6f') + "_run" + str(Run.run_counter))
        self.project = project
        self.checksum_method = "MD5"

        # Md5sums are provided by user
        if(project.md5sums):
            filename1 = os.path.basename(file1)
            self.file1_md5 = project.md5sums[filename1]
            if(file2):
                filename2 = os.path.basename(file2)
                self.file2_md5 = project.md5sums[filename2]
        # Calculate and store md5sums
        else:
            self.file1_md5 = self.md5(file1)
            if(file2):
                self.file2_md5 = self.md5(file2)

        self.filetype = filetype

    @staticmethod
    def md5(fname):
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()
