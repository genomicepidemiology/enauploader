from .min_check import ENAMinCheck


class GMIMinCheck(ENAMinCheck):

    def __init__(self, project):
        ENAMinCheck.__init__(self, project=project)

        self.checklist_name = "GMI minimum metadata"

        # Sample specific checks
        for i, sample in enumerate(project.sample_order):
            if not sample.isolation_source:
                self.report_error("Isolation source not provided.", sample)

            if not sample.collected_by:
                self.report_error("Collected by data not provided.", sample)

            if not sample.collection_date:
                self.report_error("Collection date not provided.", sample)

            if not sample.geo_loc_name:
                self.report_error("Country not provided.", sample)

            if not sample.sci_name:
                self.report_error("Organism name was either not provided or could not be recognised.", sample)

            if sample.host and not sample.host_status:
                self.report_error("Host was specified, but the host status has not been provided.", sample)

            if not sample.strain:
                self.report_error("Strain not provided.", sample)
