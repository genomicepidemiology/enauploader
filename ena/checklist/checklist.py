from .report import SampleReport


class ENAChecklist(dict):

    def __init__(self, project):
        self.project = project
        self.not_passed = None
        self.error_count = 0
        self.passed_count = 0
        self.failed_count = 0
        self.checklist_name = None

    def report_error(self, message, sample=None):
        self.error_count += 1

        if sample:
            if sample.name in self:
                self[sample.name].messages.append(message)
            else:
                self[sample.name] = SampleReport(message=message)
        else:
            if "project" in self:
                self["project"].messages.append(message)
            else:
                self["project"] = SampleReport(message=message)

    def report_to_str(self):
        report_str = ""
        if "project" in self:
            report_str = "metadata-check-project:"
            msg = ";".join(self["project"].messages)
            report_str += msg + "\n"

        for i, sample in enumerate(self.project.sample_order):
            if sample.name in self:
                report_str += "metadata-check-row-" + str(i + 1) + ":"
                msg = ";".join(self[sample.name].messages)
                report_str += msg + "\n"

        return report_str
