from .checklist import ENAChecklist


class ENAMinCheck(ENAChecklist):

    def __init__(self, project):
        ENAChecklist.__init__(self, project=project)

        self.checklist_name = "Minimum"

        # Project checks
        if not project.release_date:
            self.report_error("No release date provided.")

        if not project.prj_type:
            self.report_error("No sample type provided (ex.: isolate or metagenomic).")

        # Sample specific checks
        for i, sample in enumerate(project.sample_order):
            sample_report = []

            if not sample.name:
                self.report_error("No sample name provided.", sample)

            # Each sample can have several experiments
            for experiment in sample.experiment:
                if not experiment.platform:
                    self.report_error("No sequence platform provided.", sample)

                if not experiment.lib_layout:
                    self.report_error("It must be specified if the library layout is paired-end or single-end.", sample)


            # Each sample can have several runs
            for run in sample.run:
                if not run.file1:
                    self.report_error("No raw read file provided.", sample)

                if run.experiment.lib_layout == "PAIRED" and not run.file2:
                    self.report_error("Paired-end sequencing specified, but no second raw read file was provided.", sample)
