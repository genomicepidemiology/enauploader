import os
import sys
import argparse
import random
import string
import gzip
import time
import requests.exceptions

sys.path.append(os.getcwd())

from ENAUploader import ENAUploader, Auth


def create_random_dna(length):
    """ Creates a pseudo random DNA string of the given length.
    """
    return ''.join(random.choice("ATCG") for _ in range(length))


def write_seq(output_file):
    """
    """
    sample_headers = [
        "@FCD1V0:7:1101:1259:2164#CCATCCT/1",
        "@FCD1V0:7:1101:2229:2138#CCATCCT/1",
        "@FCD1V0:7:1101:3467:2150#CCATCCT/1",
        "@FCD1V0:7:1101:3674:2021#CCATCCT/1",
        "@FCD1V0:7:1101:1259:2164#CCATCCT/2",
        "@FCD1V0:7:1101:2229:2138#CCATCCT/2",
        "@FCD1V0:7:1101:3467:2150#CCATCCT/2",
        "@FCD1V0:7:1101:3674:2021#CCATCCT/2"
    ]

    sample_fw = ""
    sample_rw = ""

    for i in range(4):
        sample_fw += sample_headers[i + 4] + "\n"
        sample_fw += create_random_dna(100) + "\n"
        sample_fw += "+" + "\n"
        sample_fw += create_random_dna(100) + "\n"

        sample_rw += sample_headers[i + 4] + "\n"
        sample_rw += create_random_dna(100) + "\n"
        sample_rw += "+" + "\n"
        sample_rw += create_random_dna(100) + "\n"

    with gzip.open(output_file + "_fw_1.fastq.gz", "wb") as fh:
        fh.write(sample_fw)

    with gzip.open(output_file + "_rw_2.fastq.gz", "wb") as fh:
        fh.write(sample_rw)


def create_meta_test(output_file, meta):
    """ Input is a list of dicts.
    """
    # Print headers
    output = "\t".join(ENAUploader.vocs) + "\n"

    for entry in meta:
        entry_list = []
        for header in ENAUploader.vocs:
            if header in entry:
                if(entry[header] is not None):
                    entry_list.append(str(entry[header]))
                else:
                    entry_list.append("")
            else:
                entry_list.append("")
        output += "\t".join(entry_list) + "\n"

    with open(output_file + "_metadata.txt", "w") as fh:
        fh.write(output)


if __name__ == '__main__':
    # Handling credentials
    # This first part is not important

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("-c", "--credentials",
                        help="File with user name and password for ENA\
                              account. If left out user must input credentials\
                              via the --ena_user and --ena_pass flags.",
                        default=None,
                        metavar="TXT_FILE")
    parser.add_argument("-eu", "--ena_user",
                        help="ENA user name. Not necessary if credentials file\
                              has been provided.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-ep", "--ena_pass",
                        help="ENA password. Not necessary if credentials file\
                              has been provided.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("--test_dir",
                        help="Create test files in the given directory.",
                        default="./test/data/",
                        metavar="PATH")
    parser.add_argument("--center",
                        help="Sequencing center. Default is DTU-GE.",
                        default="DTU-GE",
                        metavar="STRING")
    parser.add_argument("--create_test_data",
                        help="Creates random seq_files along with a metadata\
                              sheet, that fits the data. These test files can\
                              be used to test the stand-alone functionality.",
                        action="store_true",
                        default=False)

    args = parser.parse_args()

    # Check credentials file.
    if args.credentials:
        ena_auth = Auth(args.credentials)
    elif args.ena_user and args.ena_pass:
        ena_auth = Auth()
        ena_auth.user = args.ena_user
        ena_auth.password = args.ena_pass

    #
    # test data
    #

    # Creating fake sequence data

    write_seq(args.test_dir + "test")
    write_seq(args.test_dir + "test2")

    sample1 = {
        "uuid": "sample1_" + format(time.time(), '.6f'),
        "file1": "test/data/test_fw_1.fastq.gz",
        "file2": "test/data/test_rw_2.fastq.gz",
        "name": "some name",
        "org": "Salmonella enterica",
        "country": "Denmark",
        "city": "Copenhagen",
        "col_dat": "2015",
        "host": "human",
        "host_status": "healthy",
        "col_by": "Me",
        "iso": "Feces",
        "sequencing_platform": "Illumina",
        "sequencing_type": "paired",
        "instrument_model": "Illumina MiSeq",
        "insert_size": 300
    }
    sample2 = {
        "uuid": "sample2_" + format(time.time(), '.6f'),
        "file1": "test/data/test2_fw_1.fastq.gz",
        "file2": "test/data/test2_rw_2.fastq.gz",
        "name": "some other name",
        "org": "Escherichia coli",
        "country": "Portugal",
        "col_dat": "2011-10-24",
        "col_by": "Someone else",
        "iso": "Slaughter house",
        "sequencing_platform": "Illumina",
        "sequencing_type": "paired",
        "instrument_model": "Illumina MiSeq"
    }
    some_db = {"sample1": sample1, "sample2": sample2}

    #
    # Create list of input data for ENAUploader object.
    #
    input_data = []

    # Populate the list with data from some database.
    for sample_id in some_db:
        sample = some_db[sample_id]
        input_data.append({
            "uuid": sample["uuid"],
            "first_file": sample["file1"],
            "second_file": sample["file2"],
            "sample_name": sample["name"],
            "isolate": sample["name"],
            "organism": sample["org"],
            "country": sample.get("country", None),
            "city": sample.get("city", None),
            "collection_date": sample["col_dat"],
            "host": sample.get("host", None),
            "host_status": sample.get("host_status", None),
            "collected_by": sample["col_by"],
            "isolation_source": sample["iso"],
            "sequencing_platform": sample["sequencing_platform"],
            "sequencing_type": sample["sequencing_type"],
            "instrument_model": sample["instrument_model"],
            "insert_size": sample.get("insert_size", None)
        })

    if(args.create_test_data):
        create_meta_test(args.test_dir + "/test", input_data)
        print("Test data written to test dir: " + args.test_dir)
        print("Possible test command:")
        print("python ENAUploader.py -o my_test --center DTU-GE --title 'Test"
              " submission' --abstract 'Testing ENAUploader' --material DNA "
              "--selection genome --broker '' --umbrella '' --type 'isolate' "
              "--release 2019-06-08 --test --ena_user <webin account> "
              "--ena_pass <webin pass> test/data/test_metadata.txt")
        quit()

    # The ENAUploader constructer has a lot of arguments to set.
    # A dict is used for readability.
    prj_data = {
        "ena_user": ena_auth.user,
        "ena_pass": ena_auth.password,
        "center": args.center,
        "prj_title": "Some Title",
        "prj_abstract": "Some abstract describing the project.",
        "prj_type": "isolate",
        "release": "2018-01-01",
        "material": "DNA",
        "selection": "genome",
        # TODO change in production or leave commented
        # "umbrella": "PRJEB6071",
        # TODO Change to False in productioon
        "validate": False,
        "broker": "DTU-GE"
    }

    # Object creation, storing project specific data.
    uploader_obj = ENAUploader(**prj_data)

    # Adding all the sample specific metadata
    try:
        uploader_obj.data(input_data)
    # Object creation requires an internet connection. A missing connection
    # will raise a requests.exceptions.ConnectionError.
    except requests.exceptions.ConnectionError as err:
        print("Submission failed.")
        print("requests.exceptions.ConnectionError")
        quit(1)

    # Validate metadata
    checklist_report = uploader_obj.validate(checklist="gmi")
    # The number of errors are recorded in the returned object.
    if checklist_report.error_count:
        print(checklist_report.report_to_str())
        quit(1)
    # If no errors are found the execution proceeds
    else:
        print("Validation successful.")
        print("Uploading...")

        # The sequence data is uploaded with the specified method, which can
        # only be FTP at the moment.
        uploader_obj.upload("FTP")
        print("Uploading... done!")

        # Creation and submission of the XML files to ENA
        # And output argument is used to tell the method where to store the
        # xml files.
        try:
            # TODO uncoment in production
            # ena_response = uploader_obj.submit_xmls(output="./metadata")
            ena_response = uploader_obj.submit_xmls(output="./metadata",
                                                    test_server=True)
        except OSError as err:
            print(err.args[0])
            uploader_obj.clean()
            quit(1)

        # Check the ENAResponse object to see if the submission was
        # successful
        if ena_response.success:
            print("Submission successful.")

            submission_accession = ena_response.accessions["SUBMISSION"]
            print("Submission accession no.: " + submission_accession)

            project_accession = ena_response.accessions["PROJECT"]
            print("Project accession no.: " + project_accession)

            for sample_id in some_db:
                # Get sample information from your own database
                sample = some_db[sample_id]
                sample_uuid = sample["uuid"]

                # Retrieve the relevant accession numbers for the sample
                accs = uploader_obj.get_rel_sample_acc(uuid=sample_uuid, ena_response=ena_response)
                print("Accessions relevant for sample " + sample_id + ":")
                # There is only one sample accession no. per sample
                print("Sample accession no.: " + accs["SAMPLE"])

                # There can be multiple experiment accessions per sample
                # Most of the time there will be just one
                print("Experiment accession no.: ")
                for exp_acc_no in accs["EXPERIMENT"]:
                    print("\t\t" + exp_acc_no)

                # There can be multiple run accessions per sample
                # Most of the time there will be just one
                print("Run accession no.: ")
                for run_acc_no in accs["RUN"]:
                    print("\t\t" + run_acc_no)
        else:
            for message in ena_response.messages:
                print(message)
            print("Submission failed.")

        # TODO comment in production
        # Delete all sequence data from ENAs server$
        uploader_obj.clean()
