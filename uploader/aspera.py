import os
import subprocess
import tempfile
import sys
import ftplib
import os.path


class AsperaENA():

    def __init__(self, ascp_path, auth, _dir, files_list, limit=20):
        """ The limit sets the maximum bandwith limit in MB/s."""

        os.environ["ASPERA_SCP_PASS"] = auth.password

        self.files = files_list
        self.server = "webin.ebi.ac.uk"
        self.dir = _dir
        self.limit = limit
        options = ("-T "  # disable encryption to increase throughput
                   "-d "  # Create dir if it doesn't exist
                   "-l " + str(limit) + "M "  # Bandwith limit in MB/s
                   "--overwrite=diff "  # Overwrite files if different
                   "--mode=send "
                   "--file-checksum=md5 "
                   "--policy=Fair "  # Bandwith policy
                   "--user=" + auth.user + " "
                   "--host=" + self.server + " "
                   + ",".join(files_list) + " "  # Source files
                   + self.dir)  # Host directory

        self.cmd = ascp_path + " " + options
        print(self.cmd)

        # self.cmd = ascp_path + "  " + self.limit + " -L- "

        # -T disable encryption to increase throughput
        # --overwrite=diff
        # --file-list=filename
        # --mode=send
        # --file-checksum=md5
        # --policy=Fair (Low)
        # --user
        # --host
        #
        # self.cmd = ascp_path + " -QTd " + self.limit + " -L- "
        # ascp -T -l300M -L- <file to upload> <Webin-N>@webin.ebi.ac.uk:.
