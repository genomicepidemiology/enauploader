import os
import subprocess
import tempfile
import sys
import ftplib
import os.path


class FTPENA():

    def __init__(self, auth, _dir, files_list):
        self.auth = auth
        self.dir = _dir
        self.files = files_list
        self.server = "webin.ebi.ac.uk"
        ftp = ftplib.FTP(self.server)

        # ftp.set_debuglevel(1)

        ftp.login(user=auth.user, passwd=auth.password)
        try:
            ftp.mkd(self.dir)
            print("Created directory: " + self.dir)
        except ftplib.error_perm:
            print("Directory already exists.")

        ftp.cwd(self.dir)
        for f in self.files:
            filename = os.path.basename(f)
            ftp.storbinary('STOR ' + filename, open(f, 'rb'))
            print("Uploaded: " + f)

        try:
            ftp.quit()
        except ftplib.all_errors:
            pass

    def clean(self):
        try:
            ftp = ftplib.FTP(self.server)
            ftp.login(user=self.auth.user, passwd=self.auth.password)
            ftp.cwd(self.dir)
            for f in self.files:
                filename = os.path.basename(f)
                ftp.delete(filename)
            ftp.cwd("/")
            ftp.rmd(self.dir)
            ftp.quit()
            print("!!\tDirectory removed.")
        except ftplib.all_errors:
            print("!! FTP clean-up failed.")
