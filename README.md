## Release information
**Updated:** 9th of June 2017

The tool has been released and should be working. We are still in the process of updating the documentation.

## Dependencies

Python 2.7

## Installation

#### Clone the repository ####

```bash
git clone https://bitbucket.org/genomicepidemiology/ENAUploader.git
```

## Test the installation

Go to the cloned directory and execute this command to run the test.

```bash
$ python test/test.py -eu ena_username -ep ena_password
```

## Running the tool

Go to the cloned directory and execute this command to create some example data.

```bash
$ python test/test.py --create_test_data
```

The test.py script will also output an example command line:
```bash
python ENAUploader.py -o my_test --center DTU-GE --title 'Test submission' --abstract 'Testing ENAUploader' --material DNA --selection genome --broker '' --umbrella '' --type 'isolate' --release 2019-06-08 --test --ena_user <webin account> --ena_pass <webin pass> test/data/test_metadata.txt
```


## Using the tool as a python module

Please check the test.py script in the test directory, it shows how to use the ENAUploader as a module in your python scripts.