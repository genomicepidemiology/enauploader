import os
import sys
import shutil
import argparse
import requests
from signal import *
from ena.auth import Auth
from ena.data_mapper.run import Run
from ena.data_mapper.sample import Sample
from ena.data_mapper.project import Project
from ena.data_mapper.experiment import Experiment
from ena.xml_mapper.exml import ENAXML
from ena.xml_mapper.run import ENARunXML
from ena.xml_mapper.response import ENAResponse
from ena.xml_mapper.project import ENAProjectXML
from ena.xml_mapper.experiment import ENAExperimentXML
from ena.xml_mapper.submission import ENASubmissionXML
from ena.xml_mapper.sample import ENASampleXML
from uploader.ftp import FTPENA
from uploader.aspera import AsperaENA


class ENAUploader():

    # Possible key/header values in input data
    voc_seq_plat = "sequencing_platform"
    voc_seq_type = "sequencing_type"
    voc_instrument = "instrument_model"
    voc_insert = "insert_size"
    voc_file1 = "first_file"
    voc_file2 = "second_file"
    voc_uuid = "uuid"
    voc_sample_name = "sample_name"
    voc_isolate = "isolate"
    voc_strain = "strain"
    voc_org = "organism"
    voc_lat = "latitude"
    voc_lon = "longitude"
    voc_country = "country"
    voc_region = "region"
    voc_city = "city"
    voc_col_date = "collection_date"
    voc_host = "host"
    voc_host_stat = "host_status"
    voc_col_by = "collected_by"
    voc_iso_src = "isolation_source"
    voc_serovar = "serovar"

    vocs = (voc_seq_plat, voc_seq_type, voc_instrument, voc_insert,
            voc_file1, voc_file2, voc_uuid, voc_sample_name, voc_isolate,
            voc_strain, voc_org, voc_lat, voc_lon, voc_country, voc_region,
            voc_city, voc_col_date, voc_host, voc_host_stat, voc_col_by,
            voc_iso_src, voc_serovar)

    def __init__(self, ena_user, ena_pass, center, prj_title,
                 prj_abstract, prj_type, release,
                 material="DNA", selection="genome", umbrella="",
                 upload_limit=20, validate=False,
                 broker=None, md5sums=None, ena_upload_dir=None,
                 checklist="gmi"):

        self.ftp_upload = None
        self.ena_auth = Auth()
        self.ena_auth.user = ena_user
        self.ena_auth.password = ena_pass
        self.ena_project = Project(center=center,
                                   title=prj_title,
                                   abstract=prj_abstract,
                                   material=material,
                                   selection=selection,
                                   prj_type=prj_type,
                                   release_date=release,
                                   validate=validate,
                                   broker=broker,
                                   umbrella_acc=umbrella,
                                   md5sums=md5sums,
                                   ena_upload_dir=ena_upload_dir)
        if(checklist == "gmi"):
            self.checklist = "ERC000029"
        elif(checklist == "minimum"):
            self.checklist = "ERC000011"

    def data(self, data):
        """ Input data should be a list of dicts.
            The keys in the dict are defined by the class variables starting
            with 'voc'
        """

        exp_id_counter = 0
        self.seq_files = []
        for sample_dict in data:

            #
            # Create experiment object
            #
            # Extract data for experiment
            table_platform = sample_dict.get(ENAUploader.voc_seq_plat, None)

            table_layout = sample_dict.get(ENAUploader.voc_seq_type, None)
            if table_layout:
                table_layout = table_layout.upper()
            if table_layout != "PAIRED" and table_layout != "SINGLE":
                print("ERROR: Sequecning layout/type must be either 'SINGLE' "
                      "or 'PAIRED'\nProvided value: " + str(table_layout))
                quit(1)

            table_model = sample_dict.get(ENAUploader.voc_instrument, None)
            table_ins_sz = sample_dict.get(ENAUploader.voc_insert, None)

            exp_id_counter += 1
            ena_experiment = Experiment(exp_id=exp_id_counter,
                                        project=self.ena_project,
                                        platform=table_platform,
                                        lib_layout=table_layout,
                                        instrument_model=table_model,
                                        insert_size=table_ins_sz)

            #
            # Create Run object
            #
            # Extract data for run
            table_file1 = sample_dict[ENAUploader.voc_file1]
            if(not os.path.exists(table_file1)):
                sys.exit("File 1 could not be found. \n\tfile: {0}"
                         .format(table_file1))
            self.seq_files.append(table_file1)
            table_file2 = sample_dict.get(ENAUploader.voc_file2, None)
            if table_file2:
                self.seq_files.append(table_file2)
                if(not os.path.exists(table_file2)):
                    sys.exit("File 2 could not be found. \n\tfile: {0}"
                             .format(table_file2))

            ena_run = Run(project=self.ena_project, file1=table_file1, file2=table_file2)

            #
            # Create sample object
            #
            # Extract data for sample

            # Unique ID must be provided.
            table_uuid = sample_dict.get(ENAUploader.voc_uuid, None)

            table_name = sample_dict.get(ENAUploader.voc_sample_name, "")

            if(not table_uuid):
                if(table_name):
                    table_uuid = table_name
                else:
                    print("!! ERROR: Sample had no " + ENAUploader.voc_uuid
                          + " and no " + ENAUploader.voc_sample_name
                          + ". All samples must have either a unique ID or"
                          + " name.")

            table_isolate = sample_dict.get(ENAUploader.voc_isolate, None)
            if not table_isolate:
                table_isolate = table_name

            table_strain = sample_dict.get(ENAUploader.voc_strain, None)
            if not table_strain:
                table_strain = table_isolate

            table_org = sample_dict.get(ENAUploader.voc_org, None)

            table_lat = sample_dict.get(ENAUploader.voc_lat, None)
            if table_lat:
                table_lon = sample_dict.get(ENAUploader.voc_lon, None)
            else:
                table_lon = None

            table_country = sample_dict.get(ENAUploader.voc_country, None)
            table_region = sample_dict.get(ENAUploader.voc_region, None)
            table_city = sample_dict.get(ENAUploader.voc_city, None)

            if table_country:
                if table_region:
                    if table_city:
                        table_geo_loc = ":".join([table_country, table_region,
                                                  table_city])
                    else:
                        table_geo_loc = ":".join([table_country, table_region])
                elif table_city:
                    table_geo_loc = ":".join([table_country, table_city])
                else:
                    table_geo_loc = table_country
            else:
                table_geo_loc = None

            table_col_dat = sample_dict.get(ENAUploader.voc_col_date, None)

            table_host = sample_dict.get(ENAUploader.voc_host, None)
            table_host_stat = sample_dict.get(ENAUploader.voc_host_stat, None)
            if table_host and table_host_stat:
                table_host_stat = table_host_stat.lower()

            table_col_by = sample_dict.get(ENAUploader.voc_col_by, None)
            table_source = sample_dict.get(ENAUploader.voc_iso_src, None)
            table_serovar = sample_dict.get(ENAUploader.voc_serovar, None)

            ena_sample = Sample(project=self.ena_project,
                                uuid=table_uuid,
                                name=table_name,
                                organism=table_org,
                                lat=table_lat,
                                lon=table_lon,
                                geo_loc_name=table_geo_loc,
                                collection_date=table_col_dat,
                                host=table_host,
                                host_status=table_host_stat,
                                collected_by=table_col_by,
                                isolation_source=table_source,
                                isolate=table_isolate,
                                strain=table_strain,
                                serovar=table_serovar)

            self.ena_project.add_sample(ena_sample, ena_experiment, ena_run)

    def validate(self, checklist):
        """ Validate metada
        """
        return self.ena_project.check_data(checklist)

    def upload(self, upload_method):
        """ Upload sequence data
        """
        if upload_method == "FTP":
            self.ftp_upload = FTPENA(auth=self.ena_auth,
                                     _dir=self.ena_project.upload_dir,
                                     files_list=self.seq_files)
        if upload_method == "ASPERA":
            print("ERROR: ASPERA has not yet been implemented.")
            quit(1)

    def submit_xmls(self, output="./metadata", test_server=False):
        """ Create XML objects and files
        """

        sample_xml = ENASampleXML(project=self.ena_project,
                                  ena_checklist_id=self.checklist)
        run_xml = ENARunXML(project=self.ena_project)
        exp_xml = ENAExperimentXML(project=self.ena_project)
        prj_xml = ENAProjectXML(project=self.ena_project)

        out_prj_xml = output + "_ena_prj.xml"
        out_exp_xml = output + "_ena_exp.xml"
        out_sample_xml = output + "_ena_sample.xml"
        out_run_xml = output + "_ena_run.xml"
        out_sub_xml = output + "_ena_sub.xml"

        sample_xml.root_tree.write(out_sample_xml)
        run_xml.root_tree.write(out_run_xml)
        exp_xml.root_tree.write(out_exp_xml)
        prj_xml.root_tree.write(out_prj_xml)

        sub_xml = ENASubmissionXML(project=self.ena_project,
                                   prj_xml=out_prj_xml,
                                   sample_xml=out_sample_xml,
                                   exp_xml=out_exp_xml,
                                   run_xml=out_run_xml)
        sub_xml.root_tree.write(out_sub_xml)

        ena_response = ENAXML.submit(auth=self.ena_auth,
                                     sub_xml=out_sub_xml,
                                     prj_xml=out_prj_xml,
                                     sample_xml=out_sample_xml,
                                     exp_xml=out_exp_xml,
                                     run_xml=out_run_xml,
                                     test_server=test_server)

        return ena_response

    def get_rel_sample_acc(self, uuid, ena_response):
        accs = {}
        accs["SAMPLE"] = ena_response.accessions["SAMPLE"][uuid]
        accs["EXPERIMENT"] = []
        accs["RUN"] = []

        # Retrieve relevant sample
        sample = self.ena_project.samples[uuid]

        # Retrieve experiment aliases and accessions related to sample
        for exp in sample.experiment:
            exp_acc = ena_response.accessions["EXPERIMENT"][exp.alias]
            accs["EXPERIMENT"].append(exp_acc)

        # Retrieve run aliases and accessions related to sample
        for run in sample.run:
            run_acc = ena_response.accessions["RUN"][run.alias]
            accs["RUN"].append(run_acc)

        return accs

    def clean(self):
        """ TODO
            Remove sequence files from the ENA ftp site,
        """
        if self.ftp_upload:
            print("!! Removing directory from ENA: " + self.ftp_upload.dir
                  + "...")
            self.ftp_upload.clean()


if __name__ == '__main__':

    #
    # Handling arguments
    #
    parser = argparse.ArgumentParser(description="")
    # Posotional arguments
    parser.add_argument("input",
                        help="Tab seperated text file containing all the\
                              metadata needed.",
                        metavar='INPUT',
                        default=None)
    # Optional arguments
    parser.add_argument("-o", "--output",
                        help="Output prefix.",
                        default="./metadata",
                        metavar="OUTPUT_PREFIX")
    parser.add_argument("-c", "--credentials",
                        help="File with user name and password for ENA\
                              account. If left out user must input credentials\
                              via the --ena_user and --ena_pass flags.",
                        default=None,
                        metavar="TXT_FILE")
    parser.add_argument("-eu", "--ena_user",
                        help="ENA user name. Not necessary if credentials file\
                              has been provided.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("-ep", "--ena_pass",
                        help="ENA password. Not necessary if credentials file\
                              has been provided.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("--center",
                        help="Sequencing center. Default is DTU-GE.",
                        default="DTU-GE",
                        metavar="STRING")
    parser.add_argument("--title",
                        help="Title of project/study.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("--abstract",
                        help="Abstract/Description of project/study.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("--material",
                        help="TODO: Find description on ENA website.",
                        default="DNA",
                        metavar="STRING")
    parser.add_argument("--selection",
                        help="TODO: Find description on ENA website.",
                        default="genome",
                        metavar="STRING")
    parser.add_argument("--broker",
                        help="Name of broker account. Default is DTU-GE. If no\
                              broker is needed, an empty string should be\
                              provided.",
                        default="DTU-GE",
                        metavar="STRING")
    parser.add_argument("--umbrella",
                        help="Umbrella accession number. Default is PRJEB6071\
                              (Acc. number used for all CGE uploads to ENA).\
                              If no umbrella accession number is needed, an\
                              empty string should be provided.",
                        default="PRJEB6071",
                        metavar="STRING")
    parser.add_argument("--type",
                        help="Specify if the uploaded data is isolate data or\
                              metagenomic data.",
                        choices=["isolate", "metagenomic"],
                        default=None)
    parser.add_argument("--release",
                        help="Specify release date. Format: YYYY-MM-DD.",
                        default=None,
                        metavar="DATE")
    parser.add_argument("--seperator",
                        help="The input table is per default assumed to be\
                              seperated by tabs. This option makes it\
                              possible to use other operators like commas,\
                              semicolons etc. However, a seperator must be\
                              a single character.",
                        default="\t",
                        metavar="STRING")
    parser.add_argument("--upload_method",
                        help="Specify which method you want to use to upload\
                              sequence data to ENA. Default is FTP. IMPORTANT:\
                              ASPERA has not yet been implemented.",
                        choices=["FTP", "ASPERA"],
                        default="FTP")
    parser.add_argument("--upload_limit",
                        help="This option is only by the ASPERA upload method.\
                              Set maximum bandwith aspera is allowed to use.\
                              Default is 20 MB/s.",
                        metavar="MB/s",
                        type=int,
                        default=20)
    parser.add_argument("--checklist",
                        help="Specify which ENA checklist to test against.\
                              Default is 'gmi'",
                        choices=["gmi", "minimum"],
                        default="gmi")
    parser.add_argument("--validate",
                        help="Validate data before submission. This feature is\
                              still beta.",
                        action="store_true",
                        default=False)
    parser.add_argument("--md5sums",
                        help="Provide md5sums for all files in submission. This\
                              will prevent the ENAuploader from calculating\
                              them, thereby saving time. The md5sum file must\
                              be formated as <md5sum>  <file>. Note the\
                              seperation with 2 spaces.",
                        default=None,
                        metavar="MD5SUMS")
    parser.add_argument("--ena_upload_dir",
                        help="Name the directory where the sequence files will\
                              be stored at the ENA FTP server.\
                              IMPORTANT: If this option is selected, the\
                              sequence files are assumed to already exist in\
                              this folder on the ENA FTP server.",
                        default=None,
                        metavar="STRING")
    parser.add_argument("--test",
                        help="Use test server.",
                        action="store_true",
                        default=False)

    args = parser.parse_args()

    # Check credentials file.
    if args.credentials:
        ena_auth = Auth(args.credentials)
    elif args.ena_user and args.ena_pass:
        ena_auth = Auth()
        ena_auth.user = args.ena_user
        ena_auth.password = args.ena_pass
    else:
        print("ERROR: Credentials file not found and no ENA user name or\
               password was provided.")
        quit(1)

    if not args.title:
        print("ERROR: Title can not be left empty.")
        quit(1)

    if not args.abstract:
        print("ERROR: Abstract can not be left empty.")
        quit(1)

    if not args.type:
        print("ERROR: Type can not be left empty.")
        quit(1)

    if not args.release:
        print("ERROR: Release date can not be left empty.")
        quit(1)

    if not args.broker:
        args.broker = None

    if not args.umbrella:
        args.umbrella = None

    #
    # Extract data from input and create objects
    #

    prj_data = {
        "ena_user": ena_auth.user,
        "ena_pass": ena_auth.password,
        "center": args.center,
        "prj_title": args.title,
        "prj_abstract": args.abstract,
        "prj_type": args.type,
        "release": args.release,
        "material": args.material,
        "selection": args.selection,
        "umbrella": args.umbrella,
        "broker": args.broker,
        "validate": args.validate,
        "md5sums": args.md5sums,
        "ena_upload_dir": args.ena_upload_dir,
        "checklist": args.checklist,
    }

    ena_project = ENAUploader(**prj_data)

    input_data = []

    with open(args.input, "r") as fh:
        header_line = fh.readline()
        header_line = header_line.rstrip()
        headers = header_line.split(args.seperator)

        header_column = {}
        for i, header in enumerate(headers):
            header_column[header] = i

        for line in fh:
            line_data = {}

            line = line.rstrip()
            if not line:
                continue

            line_list = line.split(args.seperator)
            # Append None to the end of the list. If a header is not
            # found in the input data it will retrieve the last
            # element (i.e. None).
            line_list.append(None)

            #
            # Extract data from each recognized column
            #
            # Note: Column header names are defined as class static class
            # variables in the ENAUploader class

            line_data[ENAUploader.voc_seq_plat] = line_list[
                header_column.get(ENAUploader.voc_seq_plat, -1)]

            table_layout = line_list[
                header_column.get(ENAUploader.voc_seq_type, -1)]

            if table_layout:
                table_layout = table_layout.upper()

            if table_layout != "PAIRED" and table_layout != "SINGLE":
                print("ERROR: Sequecning layout/type must be either 'SINGLE' "
                      "or 'PAIRED'")
                quit(1)

            line_data[ENAUploader.voc_seq_type] = table_layout

            line_data[ENAUploader.voc_instrument] = line_list[
                header_column.get(ENAUploader.voc_instrument, -1)]

            line_data[ENAUploader.voc_insert] = line_list[
                header_column.get(ENAUploader.voc_insert, -1)]

            line_data[ENAUploader.voc_file1] = line_list[
                header_column.get(ENAUploader.voc_file1, -1)]

            line_data[ENAUploader.voc_file2] = line_list[
                header_column.get(ENAUploader.voc_file2, -1)]

            line_data[ENAUploader.voc_uuid] = line_list[
                header_column.get(ENAUploader.voc_uuid, -1)]

            line_data[ENAUploader.voc_sample_name] = line_list[
                header_column.get(ENAUploader.voc_sample_name, -1)]

            line_data[ENAUploader.voc_isolate] = line_list[
                header_column.get(ENAUploader.voc_isolate, -1)]

            line_data[ENAUploader.voc_strain] = line_list[
                header_column.get(ENAUploader.voc_strain, -1)]

            line_data[ENAUploader.voc_org] = line_list[
                header_column.get(ENAUploader.voc_org, -1)]

            table_lat = line_list[
                header_column.get(ENAUploader.voc_lat, -1)]
            if table_lat:
                table_lon = line_list[
                    header_column.get(ENAUploader.voc_lon, -1)]
            else:
                table_lon = None

            line_data[ENAUploader.voc_lat] = table_lat
            line_data[ENAUploader.voc_lon] = table_lon

            table_country = line_list[
                header_column.get(ENAUploader.voc_country, -1)]

            line_data[ENAUploader.voc_country] = table_country

            table_region = line_list[
                header_column.get(ENAUploader.voc_region, -1)]

            line_data[ENAUploader.voc_region] = table_region

            table_city = line_list[
                header_column.get(ENAUploader.voc_city, -1)]

            line_data[ENAUploader.voc_city] = table_city

            line_data[ENAUploader.voc_col_date] = line_list[
                header_column.get(ENAUploader.voc_col_date, -1)]

            table_host = line_list[
                header_column.get(ENAUploader.voc_host, -1)]
            table_host_stat = line_list[
                header_column.get(ENAUploader.voc_host_stat, -1)]
            if table_host and table_host_stat:
                table_host_stat = table_host_stat.lower()
                if (table_host_stat and
                    table_host_stat != "healthy" and
                    table_host_stat != "diseased" and
                    table_host_stat != "not applicable" and
                    table_host_stat != "not collected" and
                    table_host_stat != "not provided" and
                        table_host_stat != "restricted access"):
                    print("ERROR: If a host is provided. The status of the"
                          "host must either be 'healthy', 'diseased', 'not "
                          "applicable', 'not collected', 'not provided', or "
                          "'restricted access'")
                    quit(1)

            line_data[ENAUploader.voc_host] = table_host
            line_data[ENAUploader.voc_host_stat] = table_host_stat

            line_data[ENAUploader.voc_col_by] = line_list[
                header_column.get(ENAUploader.voc_col_by, -1)]

            line_data[ENAUploader.voc_iso_src] = line_list[
                header_column.get(ENAUploader.voc_iso_src, -1)]

            line_data[ENAUploader.voc_serovar] = line_list[
                header_column.get(ENAUploader.voc_serovar, -1)]

            input_data.append(line_data)

    try:
        ena_project.data(input_data)
    # Object creation requires an internet connection. A missing connection
    # will raise a requests.exceptions.ConnectionError.
    except requests.exceptions.ConnectionError as err:
        print("Submission failed.")
        print("Please check your internet connection.")
        quit(1)

    # Check input data and report errors
    report = ena_project.validate(checklist=args.checklist)
    # The number of errors are recorded in the returned object.
    if report.error_count:
        print(report.report_to_str())
        print("!! Metadata contained errors.")
        print("!! Submission has been cancelled.")
        quit(1)

    print("Validation successful.")

    if(args.ena_upload_dir):
        print("ENA upload dir provided: " + args.ena_upload_dir)
        print("Skipping Upload.")
    else:
        print("Uploading...")

        if args.upload_method == "ASPERA":
            print("ERROR: ASPERA has not yet been implemented.")
            print("Switching to FTP")
            args.upload_method = "FTP"

        if args.upload_method == "FTP":
            ena_project.upload("FTP")
            print("Uploading... done!")

    print("Submitting to ENA...")

    try:
        ena_response = ena_project.submit_xmls(output=args.output, test_server=args.test)
    except OSError as err:
        print(err.args[0])
        uploader_obj.clean()
        quit(1)

    #
    # Handle ENA response & print accessions
    #

    if(args.validate and ena_response.success):
        print("Validation successful.")
    elif ena_response.success:
        print("Submission successful.")

        submission_accession = ena_response.accessions["SUBMISSION"]
        print("Submission accession no.: " + submission_accession)

        project_accession = ena_response.accessions["PROJECT"]
        print("Project accession no.: " + project_accession)

        accession_output = "Submission acc.: " + submission_accession + "\n"
        accession_output += "Project acc.: " + project_accession + "\n\n"

        accession_output += "ID\tSample\tExperiment\tRun\n"

        for line_data in input_data:
            # Get sample information from your own database
            sample_uuid = line_data[ENAUploader.voc_uuid]

            # Retrieve the relevant accession numbers for the sample
            accs = ena_project.get_rel_sample_acc(
                uuid=sample_uuid, ena_response=ena_response)

            # There is only one sample accession no. per sample
            accession_output += sample_uuid + "\t" + accs["SAMPLE"] + "\t"

            # There can be multiple experiment accessions per sample
            # Most of the time there will be just one
            accession_output += ",".join(accs["EXPERIMENT"]) + "\t"

            # There can be multiple run accessions per sample
            # Most of the time there will be just one
            accession_output += ",".join(accs["RUN"]) + "\n"

        with open(args.output + "_acc_numbers.txt", "w") as fh:
            fh.write(accession_output)

        print("Wrote: " + args.output + "_acc_numbers.txt")
    else:
        for message in ena_response.messages:
            print(message)
        print("!! Submission failed.")
